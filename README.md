# MP4Info
不用流媒体也可以简单实现MP4等视频的边下边播功能。
- - -
### 使用说明
```
/**
 * MP4Info 示例程序
 * @author TJbaobao
 *=====================原理说明:http://blog.csdn.net/u013640004/article/details/53573864
 *=====================MP4Info工具下载:http://shouji.baidu.com/software/10519592.html
 *=====================我的网站:www.imczm.com
 *=====================播放器正在弄，快了
 */
public class Main {
	public static void main(String[] args) {
		//============================切割视频文件开始===========================
		//构建自定义数据
		CustomData customData = new CustomData();
		customData
		.addData(CustomData.DATA_TYPE_HEAD_SIZE)//添加内置变量
		.addData("name", "test.mp4")//添加字符串
		.addData("abc", "wo ai jia zhu")
		;
		Mp4Helper mp4HelperMentation = new Mp4Helper();
		//添加进度监听器
		mp4HelperMentation.setOnProgressListener(new OnProgressListener() {
			@Override
			public void onProgress(float readed, float size) {
				System.out.println("切割进度:"+readed/size);
			}
		});
		//开始切割视频
		ArrayList<String> pathList = 
				mp4HelperMentation.segmentation("video/test.mp4", "video",customData);//视频路径、输出文件夹、自定义数据结构体
		//===========================切割视频文件结束==============================
		
		
		//===========================合并视频文件开始==============================
		Mp4Helper mp4HelperMerger = new Mp4Helper();
		//设置合并进度监听器
		mp4HelperMerger.setOnProgressListener(new OnProgressListener() {
			@Override
			public void onProgress(float readed, float size) {
				System.out.println("合并进度:"+readed/size);
			}
		});
		//传入tjbb格式头文件路径，开始读取必须信息
		Mp4Merger mp4Merger = mp4HelperMerger.mergeInfo(pathList.get(0), "video/test_merge.mp4");//tjbb格式头文件路径，合成的视频输出地址
		//获取自定义数据结构体
		CustomData customDataGet = mp4Merger.getCustomData();
		if(customDataGet!=null)
		{
			System.out.println("name:"+customDataGet.getStringValue("name"));
		}
		//添加视频文件的数据部分
		int i = 0;
		for(String path:pathList)
		{
			if(i!=0)
			{
				mp4Merger.addMDat(path);
			}
			i++;
		}
		//===========================合并视频文件结束==============================
	}
}
```
### 完整的DEMO示例，包括录像和播放一整套
**[VideoPlayOL](https://gitee.com/tjbaobao/VideoPlayOL)**
正在编写中。。。暂不开放(加班累成狗，没时间写)

