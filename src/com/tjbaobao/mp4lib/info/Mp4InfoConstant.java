package com.tjbaobao.mp4lib.info;

public class Mp4InfoConstant {
	public static final String HEAD_NAME = "tjbb";
	public static final String HEAD_CUSTOM_NAME = "cust";
	public static final String HEAD_CUSTOM_NAME_NAME = "name";
	public static final String HEAD_CUSTOM_NAME_VALUE_NAME = "valu";
	public static final String HEAD_CUSTOM_DATA_NAME = "data";
	public static final String HEAD_HEAD_NAME = "head";
	public static final String HEAD_FOOT_NAME = "foot";
	public static final String HEAD_MDAT_SIZE_NAME = "mdat";
	
}
