package com.tjbaobao.mp4lib.info;

import com.tjbaobao.mp4lib.tools.FileTools;
import com.tjbaobao.mp4lib.tools.HexConvertTools;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class CustomData {
	
	private static final String DEF_CHARSET_NAME= "iso8859-1";
	
	private static final int DATA_TYPE_ORDER = 0;
	public static final int DATA_TYPE_HEAD_INDEX = 1;
	public static final int DATA_TYPE_HEAD_SIZE = 2;
	public static final int DATA_TYPE_MDAT_INDEX = 3;
	public static final int DATA_TYPE_MDAT_SIZE = 4;
	public static final int DATA_TYPE_FOOT_INDEX = 5;
	public static final int DATA_TYPE_FOOT_SIZE = 6;
	
	public static final String DATA_NAME_HEAD_INDEX = "headindex";
	public static final String DATA_NAME_HEAD_SIZE = "headsize";
	public static final String DATA_NAME_MDAT_INDEX = "mdatindex";
	public static final String DATA_NAME_MDAT_SIZE = "mdatsize";
	public static final String DATA_NAME_FOOT_INDEX = "footindex";
	public static final String DATA_NAME_FOOT_SIZE = "footsize";
	
	private ArrayList<Data> customDataList = new ArrayList<>();
	private int size = 0;
	public class Data{
		int dataType = DATA_TYPE_ORDER;
		long dataSize = 0;
		String dataName = null;
		byte[] dataByte = null;
		public int getDataType() {
			return dataType;
		}
		public void setDataType(int dataType) {
			this.dataType = dataType;
		}
		public long getDataSize() {
			return dataSize;
		}
		public void setDataSize(long dataSize) {
			this.dataSize = dataSize;
		}
		public byte[] getDataByte() {
			return dataByte;
		}
		public void setDataByte(byte[] dataByte) {
			this.dataByte = dataByte;
		}
		public String getDataName() {
			return dataName;
		}
		public void setDataName(String dataName) {
			this.dataName = dataName;
		}
		
		
	}
	
	public CustomData addData(int dataType)
	{
		Data data = new Data();
		data.setDataType(dataType);
		
		switch(dataType)
		{
			case DATA_TYPE_HEAD_INDEX:
				data.setDataName(DATA_NAME_HEAD_INDEX);
				break;
			case DATA_TYPE_HEAD_SIZE:
				data.setDataName(DATA_NAME_HEAD_INDEX);
				break;
			case DATA_TYPE_MDAT_INDEX:
				data.setDataName(DATA_NAME_MDAT_INDEX);
				break;
			case DATA_TYPE_MDAT_SIZE:
				data.setDataName(DATA_NAME_MDAT_SIZE);
				break;
			case DATA_TYPE_FOOT_INDEX:
				data.setDataName(DATA_NAME_FOOT_INDEX);
				break;
			case DATA_TYPE_FOOT_SIZE:
				data.setDataName(DATA_NAME_FOOT_SIZE);
				break;
			default :
				return null;
				
		}
		size+=4;
		customDataList.add(data);
		return this;
	}
	
	public CustomData addData(String name,String values)
	{
		Data data = new Data();
		data.setDataType(DATA_TYPE_ORDER);
		try {
			byte[] bytes = values.getBytes(DEF_CHARSET_NAME);
			data.setDataByte(bytes);
			data.setDataName(name);
			data.setDataSize(bytes.length);
			size+=bytes.length;
			customDataList.add(data);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this;
	}
	/**
	 * 添加数据
	 * @param name
	 * @param bytes
	 * @return
	 */
	public CustomData addData(String name,byte[] bytes)
	{
		Data data = new Data();
		data.setDataType(DATA_TYPE_ORDER);
		data.setDataByte(bytes);
		data.setDataName(name);
		data.setDataSize(bytes.length);
		customDataList.add(data);
		return this;
	}
	
	public ArrayList<Data> getCustomDataList() {
		return customDataList;
	}
	public static int getDataTypeOrder() {
		return DATA_TYPE_ORDER;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public byte[] getValue(String name)
	{
		if(name==null)
		{
			return null;
		}
		for(Data data:customDataList)
		{
			if(data.getDataName().equals(name)&&data.getDataType()==DATA_TYPE_ORDER)
			{
				return data.getDataByte();
			}
		}
		return null;
	}
	public long getValue(int dataType)
	{
		if(dataType==DATA_TYPE_ORDER)
		{
			return 0;
		}
		for(Data data:customDataList)
		{
			if(data.getDataType()==dataType)
			{
				HexConvertTools.bytesToInt(data.getDataByte());
			}
		}
		return 0;
	}
	public String getStringValue(String name)
	{
		byte[] bytes = getValue(name);
		return HexConvertTools.bytesToStr(bytes);
	}
}
