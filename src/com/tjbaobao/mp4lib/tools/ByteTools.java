package com.tjbaobao.mp4lib.tools;

public class ByteTools {
	/**
	 * 将子字节数组复制到父字节数组，并保持相对位置不变
	 * @param parentBytes 父字节数组
	 * @param subBytes 子字节数组
	 * @param beginPosition 开始位置
	 * @param size 最小长度
	 * @return
	 */
	public static byte[] copyBytes(byte[] parentBytes,byte[] subBytes,int beginPosition,int size )
	{
		for(int i=0;i<size;i++)
		{
			int length = subBytes.length;
			if(i>=size-length)
			{
				parentBytes[i+beginPosition]=subBytes[i-(size-length)];
			}
		}
		return parentBytes;
	}
	
	public static byte[] readBytes(byte[] parentBytes,int beginPosition,int size)
	{
		byte[] bytes = new byte[size];
		for(int i=0;i<size;i++)
		{
			bytes[i] = parentBytes[beginPosition+i];
		}
		
		return bytes;
	}
	
	
}
