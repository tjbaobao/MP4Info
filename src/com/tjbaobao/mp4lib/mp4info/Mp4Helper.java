package com.tjbaobao.mp4lib.mp4info;

import com.tjbaobao.mp4lib.info.CustomData;
import com.tjbaobao.mp4lib.tools.FileTools;

import java.util.ArrayList;

public class Mp4Helper {
	
	private OnProgressListener onProgressListener;
	public static String VERSION = "V1.0.2";
	public boolean isDubug = false;
	public static Mp4Helper mp4Helper = null;
	
	public Mp4Helper()
	{
		if(isDubug)
		{
			System.out.println("VERSION="+VERSION);
		}
	}
	
	public static Mp4Helper getInstance()
	{
		if(mp4Helper==null)
		{
			mp4Helper = new Mp4Helper();
		}
		return mp4Helper;
	}
	
	/**
	 * 切割视频
	 * @param path 视频地址
	 * @return
	 */
	public ArrayList<String> segmentation(String path,String outFolder)
	{
		return segmentation(path, outFolder, Mp4Segmentation.SUB_SIZE);
	}
	public ArrayList<String> segmentation(String path,String outFolder,CustomData customData)
	{
		return segmentation(path, outFolder, Mp4Segmentation.SUB_SIZE,customData);
	}
	public ArrayList<String> segmentation(String path,String outFolder,long subSize)
	{
		return segmentation(path, outFolder, subSize, null);
	}
	public ArrayList<String> segmentation(String path,String outFolder,long subSize,CustomData customData)
	{
		if(path==null|| FileTools.getPrefix(path)==null)
		{
			return null;
		}
		Mp4Segmentation mp4Segmentation= new Mp4Segmentation();
		mp4Segmentation.setOnProgressListener(onProgressListener);
		return mp4Segmentation.segmentation(path, outFolder, subSize,customData);
	}
	
	Mp4Merger mp4Merger = null;
	/**
	 * 合并视频
	 * @param pathList 全部文件列表，指定第一个为头文件
	 * @return
	 */
	public Mp4Merger merge(ArrayList<String> pathList,String outPath)
	{
		if(pathList==null||outPath==null||FileTools.getPrefix(outPath)==null)
			return null;
		int i = 0;
		
		for(String pathMdatPart:pathList)
		{
			if(i==0)
			{
				mp4Merger = Mp4Merger.build(pathList.get(0), outPath,onProgressListener);
				mp4Merger.setOnProgressListener(onProgressListener);
				
			}
			else
			{
				mp4Merger.addMDat(pathMdatPart);
			}
			i++;
		}
		return mp4Merger;
	}
	
	/**
	 * 合并视频
	 * @param pathTjbb 头文件地址
	 * @param outPath 输出地址
	 * @return
	 */
	public Mp4Merger mergeInfo(String pathTjbb,String outPath)
	{
		mp4Merger = Mp4Merger.build(pathTjbb, outPath,onProgressListener);
		mp4Merger.setOnProgressListener(onProgressListener);
		return mp4Merger;
	}
	
	public OnProgressListener getOnProgressListener() {
		return onProgressListener;
	}

	public void setOnProgressListener(OnProgressListener onProgressListener) {
		this.onProgressListener = onProgressListener;
	}

	public void setDubug(boolean isDubug) {
		this.isDubug = isDubug;
	}
}
/*
 * 更新日志 v1.0.2
 * 修复一个必然会出现的BUG，确实是我疏漏了，很抱歉
 * 
 * 
 */
